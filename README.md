# pySimi.Data

pysimi.data is a python package making it easier to access data from Simi Motion Project files. Two different kinds of
Simi Motion Project files exist, SMP files which use an internal binary structure and XMP, which are XML based.

Accessing an SMP file is only possible on a Windows System and the SIMIData component is installed.

For more details and information, visit https://gitlab.com/simi-rms/pysimi.data/-/wikis/home

# Installation 
To install use pip:

    $ pip install "git+https://gitlab.com/simi-rms/pysimi.data"

Or clone the repo:

    $ git clone https://gitlab.com/simi-rms/pysimi.data.git
    $ python setup.py install

# Synopsis

## Iterating over all data groups and data rows    

    import pysimi.data as sd
    
    prj = sd.Project(r'athlete.smp')
    
    for data_group in prj.walk_data_group():
        for data_row in data_group.walk_data_row():
            print("{}/{}".format(data_group.name, data_row.name))

## Retrieving known Element and plotting it

    import matplotlib.pyplot as plt
    import pysimi.data as sd
    
    prj = sd.Project(r'athlete.smp')
    
    data_row = prj.find_data_row(data_group_id=200613, data_row_id=7670)
    
    print("Name...........: {}".format(data_row.name))
    
    y = data_row.get_all_data_rows()
    plt.plot(y)
    plt.show()

## Retrieving known Magic data and plotting it

    import matplotlib.pyplot as plt
    import pysimi.data as sd

    prj = sd.Project(r'athlete.smp')

    data_group = prj.get_data_group(sd.MagicKeys.FPLC)
    print(len(data_group), 'Number of FPLC Data Groups')

    data_row = data_group[0].get_data_row_by_index(0, 3)

    data = data_row.get_all_data_rows()
    print(data_group[0].name, '/', data_row.name, '/', data)

    plt.plot(data)
    plt.show()

# Detailed Description

A Simi Motion Project will accumulate data groups following a data collection. These data groups contain data rows,
which typically represent all data of a certain type. Each data row may then have many data tracks. Data tracks are
the object containing time-series data.

For example, a data group named 3D coordinates is created in the Simi Motion Project. This data group is identified by
a specific ID, and hosts data rows of type (aka Magic) [-3D-]. There are data rows in the data group named L.PSIS,
L.Heel, C7, etc, and are identified by a specific ID. Each data row then contains data tracks, where track_index=0
is the X-coordinate, track_index=1 is the Y-coordinate, and so on.

For example:
    DataGroup = Joint Centers [3D-U] 200609
        # DataGroup: Name [magic] ID
    DataRow = Pelvis 9007 0
        # DataRow: Name ID Index
    Tracks = X-coordinate [0 of 3]
        # Pelvis has 4 tracks: X, Y, Z, and path length

DataGroups, DataRows, and Tracks are all zero-base indexed and can be visualized by hovering the cursor over data
groups in the Simi Motion Project.


# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.projectbase import ProjectBase


class PhaseRow:

    def __init__(self, project: ProjectBase, impl_parent, impl_data, name: str, start: float, end: float):
        """
        Create a PhaseRow element object.

        This should never be called directly! Use a PhaseGroup element for obtaining a PhaseRow element.

        :type project: The owning project object. It will be used for getting PhaseRow elements.
        :type impl_parent: The underlying PhaseGroup implementations, which is the parent of this PhaseRow element.
        :type impl_data: The underlying PhaseRow implementation. It is needed by the owning project as reference.
        :type name: User specified name of this PhaseRow element.
        """
        self._project = project
        self._impl_parent = impl_parent
        self._impl_data = impl_data
        self._name = name
        self._start = start
        self._end = end

    @property
    def name(self):
        """
        Name of the PhaseRow element
        :type: str
        """
        return self._name

    @property
    def start(self):
        """
        Start time of the PhaseRow element
        :type: str
        """
        return self._start

    @property
    def end(self):
        """
        End time of the PhaseRow element
        :type: str
        """
        return self._end

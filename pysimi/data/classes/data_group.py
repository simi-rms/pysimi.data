# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.magickeys import MagicKey
from pysimi.data.classes.projectbase import ProjectBase


class DataIterator:
    """
    DataIterator is an iterator class for looping used in walk_data_row.
    """

    def __init__(self, project, group, data_row_count, track_index):
        self._project = project
        self._group = group
        self._data_row_count = data_row_count
        self._track_index = track_index
        self._index = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self._index < self._data_row_count:
            data_row = self._project.get_data_row_by_index(self._group, self._track_index, self._index)
            self._index += 1
        else:
            raise StopIteration()
        return data_row


class EmptyDataIterator:
    """
    EmptyDataIterator is an iterator class for faking an empty DataGroup.
    """

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration()


class DataGroup:
    """
    DataGroup is a container for multiple data rows.
    """

    def __init__(self, project: ProjectBase, impl_data, id: int, magick: MagicKey, name: str, track_count: int,
                 data_row_count: int):
        """
        Create a DataGroup object.

        :param project: The owning project object. It will be used for getting DataRow elements.
        :param impl_data: The underlying DataGroup implementation. It is needed by the owning project as reference.
        :param id: The ID of the DataGroup element.
        :param magick: The magic key of the DataGroup element.
        :param name: The user defined name of the DataGroup element.
        :param track_count: The number of tracks the DataGroup element has.
        :param data_row_count: The number of DataRow element this DataGroup element has.
        """
        self._project = project
        self._impl_data = impl_data
        self._id = id
        self._magick = magick
        self._name = name
        self._track_count = track_count
        self._data_row_count = data_row_count

        # TODO: Do really need these elements? The are part the SmProject aka C++ implementation. The question is
        #       will users actually use them and what is provided by the XML data.
        # self._user_data_size
        # self._magic_data
        # self._interpolation_type

    @property
    def id(self):
        """
        ID of the DataGroup element.
        :type: int
        """
        return self._id

    @property
    def magick(self):
        """
        MagicKey of the DataGroup element.
        :type: MagicKey
        """
        return self._magick

    @property
    def name(self):
        """
        Name of the DataGroup element.
        :type: MagicKey
        """
        return self._name

    @property
    def track_count(self):
        """
        Number of  track ...
        :type: int
        """
        return self._track_count

    @property
    def data_row_count(self):
        """
        Number of the DataRow elements.

        :type: int
        """
        return self._data_row_count

    def get_data_row_by_index(self, track_index: int, data_row_index: int):
        """
        Returns a DataRow element.

        :param track_index: Track index of the DataRow element.
        :param data_row_index: DataRow index of the DataRow element.
        :return: DataRow element
        :type: DataRow
        """
        if not track_index < self._track_count:
            raise IndexError("requested track index: {}; max track index: {}".format(track_index, self._track_count - 1))
        if not data_row_index < self._data_row_count:
            raise IndexError("DataGroup has {} data element, but {} was requested".format(self._data_row_count, data_row_index))

        return self._project.get_data_row_by_index(self._impl_data, track_index, data_row_index)

    def get_data_row_by_id(self, track_index: int, data_row_id: int):
        """
        Returns a DataRow element.

        (?) Are the ID fixed or known in some way? If not it does not really make sense to have this method.

        :param track_index: Track index of the DataRow element.
        :param data_row_id: ID of the DataRow element.
        :return: DataRow element
        :type: DataRow
        """
        if not track_index < self._track_count:
            raise IndexError("requested data index: {}; max data index: {}".format(track_index, self._track_count - 1))

        return self._project.get_data_row_by_id(self._impl_data, track_index, data_row_id)

    def walk_data_row(self, track_index=0):
        """
        return an iterator, which enables the user to iterate over DataRow elements.

        Example::
            for ds in project.walk_data_row():
                print([ds.name, ds.id])

        :param track_index: index of the data tracks that are returned
        :return:
        """
        if self._track_count == 0:
            return EmptyDataIterator()

        if not track_index < self._track_count:
            raise IndexError("requested track index: {}; max track index: {}".format(track_index, self._track_count - 1))

        return DataIterator(self._project, self._impl_data, self._data_row_count, track_index)

# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pathlib import Path


class ProjectBase:
    """
    The ProjectBase class is the base class, of all classes which are handled by Project class and needs to interact
    with the DataGroup and DataRow classes.
    """

    def __init__(self, path):
        # Allowing None makes writing unit test simpler
        self.path = Path(path) if path else None
        pass

    def get_data_row_by_index(self, impl_data, track_index: int, data_row_index: int):
        # The total number of Data elements should be the same across the different implementations. But the index of
        # the Data elements may vary between the different implementations.
        raise NotImplementedError("get_data_row_by_index(...)")

    def get_data_row_by_id(self, impl_data, track_index: int, data_row_id: int):
        # The total number of Data elements should be the same across the different implementations. But the index of
        # the Data elements may vary between the different implementations.
        raise NotImplementedError("get_data_row_by_id(...)")

    def get_data_row(self, impl_data):
        raise NotImplementedError("get_data_row(...)")

    def get_all_data_rows(self, group, data_row_id: int):
        raise NotImplementedError("get_all_data_rows(...)")

    def get_phase_row_by_index(self, impl_data, phase_row_index):
        raise NotImplementedError("get_phase_row_by_index(...)")


import platform
from pathlib import Path
from typing import Union

from pysimi.data.classes.magickeys import MagicKey
from pysimi.data.classes.xmproject import XmProject
from pysimi.data.classes.smproject import SmProject


class Project:
    """
    Base class to handle Simi project files (.xmp or .smp)

    Load an internal project based on the file type.
    Contains several functions to read and assess data independent from file type.
    """
    def __init__(self, path: Union[Path, str], always_smp: [bool] = False):
        """
        Create a project by loading a Simi project file of type .smp or .xmp
        :param path: absolute path to the simi project file
        """
        path = Path(path)
        if self.smp_file_exists(path) or (always_smp == True and self.xmp_file_exists(path)):
            self.internal_project = SmProject(path)
        elif self.xmp_file_exists(path):
            self.internal_project = XmProject(path)
        elif path.is_file():
            raise ValueError('File type is not defined!: ' + str(path))
        else:
            raise FileNotFoundError('No such file or directory: ' + str(path))

    @staticmethod
    def is_file_type(path: Union[Path, str], file_type: str):
        """
        Check if a given file has correct file type, i.e. video.avi has file_type .avi

        :param path: full path to the file to check
        :param file_type: style: '.type' (dot needs to be included)
        :return: bool
        """
        path = Path(path)
        return path.suffix.lower() == file_type.lower()

    @staticmethod
    def smp_file_exists(path: Union[Path, str]):
        """
        Check existence of smp file

        :param path: absolute path to file to check
        :return: bool
        """
        path = Path(path)
        return Project.is_file_type(path, '.smp') and path.is_file()

    @staticmethod
    def xmp_file_exists(path: Union[Path, str]):
        """
        Check existence of xmp file

        :param path: absolute path to file to check
        :return: bool
        """
        path = Path(path)
        return Project.is_file_type(path, '.xmp') and path.is_file()

    @staticmethod
    def smp2xmp(smp_path: Union[Path, str], xmp_path: Union[Path, str] = None):
        """
        Convert smp file to xmp. Only callable on windows systems

        :param smp_path: absolute path to smp file
        :param xmp_path: output path + new file name; if not set, the smp path & name are taken as default
        :return: None
        """
        smp_path = Path(smp_path)
        if not Project.smp_file_exists(smp_path):
            raise FileNotFoundError('No such file or directory: ' + str(smp_path))
        if platform.system() != 'Windows':
            raise OSError('This function is only implemented for windows')

        if xmp_path is None:
            xmp_path = smp_path.parent / Path(smp_path.stem + '.xmp')

        if SmProject.smlib is None:
            raise RuntimeError("Could not load SIMIData")
        smp = SmProject.smlib.OpenProject(smp_path)
        smp.SaveXMLFile(xmp_path, 0)
        return

    @staticmethod
    def xmp2smp(xmp_path: Union[Path, str], smp_path: Union[Path, str] = None):
        """
        Convert xmp file to smp. Only callable on windows systems

        :param xmp_path: absolute path to xmp file
        :param smp_path: output path + new file name; if not set, the xmp path & name are taken as default
        :return: None
        """
        xmp_path = Path(xmp_path)
        if not Project.xmp_file_exists(xmp_path):
            raise FileNotFoundError('No such file or directory: ' + str(xmp_path))
        if platform.system() != 'Windows':
            raise OSError('This function is only implemented for windows')

        if smp_path is None:
            smp_path = xmp_path.parent / Path(xmp_path.stem + '.smp')

        if SmProject.smlib is None:
            raise RuntimeError("Could not load SIMIData")
        smp = SmProject.smlib.CreateProject(0)
        smp.LoadXMLFile(xmp_path)
        smp.Save(smp_path)
        return

    @staticmethod
    def search_file_recursively(root_path: Union[Path, str], file_path: Union[Path, str]):
        """
        Starting from the root path search subdirectories for given file.
        Concatenate the root path with parent folders from file path and check if file exists.
        Check only for given parent folders from file_path and not all subdirectories of root_path

        :param root_path: base directory the search starts in
        :param file_path: absolute path to searched file
        :return: updated file path if found, else the input file_path
        """
        file_path = Path(file_path)
        root_path = Path(root_path)
        if file_path.is_file():
            return file_path
        if root_path.is_file():
            root_path = root_path.parent
        file_dir, file_name = file_path.parent, file_path.name
        recursive_folders = list(file_dir.parts)
        recursive_folders.append("")
        recursive_folders.reverse()
        recursive_path = Path("")
        for folder in recursive_folders:
            recursive_path = Path(folder) / recursive_path
            check_path = root_path / recursive_path / file_name
            if check_path.is_file():
                return check_path
        return file_path

    def get_videos(self, path_update=True):
        """
        Provide video file paths for camera objects in internal project

        :param path_update: if true, try to find an updated file path relative to the location of the Simi project file.
        Can only be found if videos are in the same folder structure as project file and folders have same naming as
        found video paths; helpful if project was moved before
        :return: list of paths to the videos
        """
        vid_paths = self.internal_project.get_videos()
        if path_update:
            vid_paths = [self.search_file_recursively(self.internal_project.path.parent, path) for path in vid_paths]
        return vid_paths

    def get_data_group(self, magic: MagicKey = None):
        """
        Return all data groups of the project.

        :param magic: If specified the return data group instances have the same magic value. If the provided value is None
        or MagicKeys.DATAS_NULL all data groups will be selected.

        :return: All found data group elements, which fulfill the provided specification.
        """
        return self.internal_project.get_data_group(magic)

    def walk_data_group(self):
        """
        return an iterator, which enables the user to iterate over DataGroup elements.

        Example::
            for ds in project.walk_data_group():
                print([ds.magic, ds.id, ds.name])
        """
        return self.internal_project.walk_data_group()

    def find_data_row(self, data_group_id: int, data_row_id: int, track_index: int = 0):
        """
        Extract DataRow track based on DataGroup id and DataRow id.

        The chosen DataRow needs to be a child element of the DataGroup otherwise an error is raised

        :param data_group_id: id of root dataGroup
        :param data_row_id: id of data row searched for
        :param track_index: index of the chosen track
        :return:
        :type: Data
        """
        for data_group in self.walk_data_group():
            if data_group.id == data_group_id:
                for data_row in data_group.walk_data_row(track_index):
                    if data_row.id == data_row_id:
                        return data_row
        return None

    def get_phase_group(self, index=0):
        """
        Return all phase groups of the project.

        :param index: index of the phase group element queried.

        :return: All found phase group elements.
        """
        return self.internal_project.get_phase_group(index)

    def walk_phase_group(self):
        """
        return an iterator, which enables the user to iterate over PhaseGroup elements.
        """
        return self.internal_project.walk_phase_group()

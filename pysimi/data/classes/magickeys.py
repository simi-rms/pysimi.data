add_unknown = None


def _encode_magic_key(cc):
    assert len(cc) == 4
    if isinstance(cc[0], int):
        return (cc[3] << 8 * 3) | (cc[2] << 8 * 2) | (cc[1] << 8 * 1) | (cc[0] << 8 * 0)
    else:
        return (ord(cc[3]) << 8 * 3) | (ord(cc[2]) << 8 * 2) | (ord(cc[1]) << 8 * 1) | (ord(cc[0]) << 8 * 0)


def _decode_magic_key(cc):
    return "".join([chr((int(cc) >> 8 * i) & 0xFF) for i in range(4)])


_magic_keys_dict = set()


def _create_key(magic, description, tracks):
    k = MagicKey(magic, description, tracks)
    if k in _magic_keys_dict:
        raise ValueError('Key "{}" is already registered'.format(k))
    _magic_keys_dict.add(k)
    return k


class MagicKey:
    def __init__(self, magic, description, tracks):
        if isinstance(magic, list):
            self.magic = _encode_magic_key(magic)
        else:
            self.magic = _encode_magic_key(list(magic))
        self.description = description
        self.tracks = tracks

    @property
    def magic_str(self):
        return _decode_magic_key(self.magic)

    def __str__(self):
        return self.magic_str + ': ' + str(self.description)

    def __repr__(self):
        return 'MagicKey({})'.format(self.magic_str)

    def __hash__(self):
        return self.magic

    def __lt__(self, other):
        if isinstance(other, MagicKey):
            return self.magic < other.magic
        elif isinstance(other, int):
            return self.magic < other
        else:
            raise TypeError("Only MagicKey or int can be compared, provided was '{}'".format(other.__class__))

    def __eq__(self, other):
        if isinstance(other, MagicKey):
            return self.magic == other.magic
        elif isinstance(other, int):
            return self.magic == other
        else:
            raise TypeError("Only MagicKey or int can be compared, provided was '{}'".format(other.__class__))


class MagicKeys:
    # TODO: Some descriptions are not added to the confluence page. These keys can be found by the description text:
    #   INSERT DESCRIPTION AND DIMENSION

    DATA_GROUP_NULL = _create_key([0, 0, 0, 0], 'Null element', 0)
    # We cannot register more than one element with the same key
    # DATA_GROUP_ANY = _create_key([0, 0, 0, 0], 'Any element', 0)
    DATA_GROUP_UNKNOWN = _create_key('????', 'Data unknown', 0)

    DATA_GROUP_AMTI = _create_key('AMTI', 'AMTI force data (deprecated)', 1)
    DATA_GROUP_EMG = _create_key('EMG-', 'EMG raw data', 1)
    DATA_GROUP_EMGCALC = _create_key('EMGC', 'EMG calculated data', 1)
    DATA_GROUP_RAW = _create_key('RAW ', 'camera raw data', 2)
    DATA_GROUP_FRAW = _create_key('FRAW', 'camera filtered raw data', 2)
    DATA_GROUP_URAW = _create_key('URAW', 'camera user-defined raw data', 2)
    DATA_GROUP_WRAW = _create_key('WRAW', 'camera wand raw data', 2)
    DATA_GROUP_GRAW = _create_key('GRAW', 'camera grid raw data', 2)
    DATA_GROUP_CRAW = _create_key('CRAW', 'Camera distortion corrected raw data', 2)
    DATA_GROUP_PMROTATION = _create_key('PMRO', 'camera pattern matching rotation raw data', 1)
    DATA_GROUP_2D = _create_key('-2D-', '2D calculated data', 2)
    DATA_GROUP_3D = _create_key('-3D-', '3D calculated data', 3)
    DATA_GROUP_ANGLES = _create_key('ANGL', '1D angle data (degrees)', 1)
    DATA_GROUP_USER = _create_key('USER', '1D user-defined data', 1)
    DATA_GROUP_2DUSER = _create_key('2D-U', '2D user-defined data', 2)
    DATA_GROUP_2DCOG = _create_key('2DCG', '2D center of gravity', 2)
    DATA_GROUP_3DUSER = _create_key('3D-U', '3D user-defined data', 3)
    DATA_GROUP_3DCOG = _create_key('3DCG', '3D center of gravity data', 3)
    DATA_GROUP_3DFV = _create_key('3DFV', '3D force vector data', 3)
    DATA_GROUP_3DROTATION = _create_key('3DRO', '3D rotational data (so(3))', 3)
    DATA_GROUP_3DACCEL = _create_key('3DAC', '3D acceleration data', 3)
    DATA_GROUP_3DANGULARV = _create_key('3DAV', '3D angular velocity data', 3)
    DATA_GROUP_3DMAGNETIC = _create_key('3DMF', '3D magnetic sensor raw data', 3)
    DATA_GROUP_FREQUENCY = _create_key('FRQU', 'frequency analysis data', 1)
    DATA_GROUP_MOMENTUM = _create_key('MOMT', 'momentum calculated data (from center of gravity)', 3)
    DATA_GROUP_FOOT = _create_key('FOOT', 'foot pressure data (custom representation)', 1)
    DATA_GROUP_CIDY = _create_key('CIDY', 'inverse dynamics', 0)

    # regular force plates (FP)

    DATA_GROUP_FRCPLTRAW = _create_key('FPLR', 'force plate raw data', 1)
    FPLR = DATA_GROUP_FRCPLTRAW
    DATA_GROUP_FRCPLTCALC = _create_key('FPLC', 'force plate calculated data', 1)
    FPLC = DATA_GROUP_FRCPLTCALC

    # treadmill with 4 force plates (vet)

    DATA_GROUP_FRCPLT4 = _create_key('FPL4', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_FRCPLT4CALC = _create_key('FP4C', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_FRCPLT4V = _create_key('F4VR', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_FRCPLT4VCALC = _create_key('F4VC', 'INSERT DESCRIPTION AND DIMENSION', -1)

    # treadmill with 2 force plates (human)

    DATA_GROUP_FRCPLT2 = _create_key('FPL2', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_FRCPLT2CALC = _create_key('FP2C', 'INSERT DESCRIPTION AND DIMENSION', -1)

    # special force plates with 2 sensors (B.Schimpl)

    DATA_GROUP_FRCPLT2SENS = _create_key('F2SR', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_FRCPLT2SENSCALC = _create_key('F2SC', 'INSERT DESCRIPTION AND DIMENSION', -1)

    # special AMTI - force plates for ski jumping

    DATA_GROUP_SKIJUMP = _create_key('SKJR', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_SKIJUMPCALC = _create_key('SKJC', 'INSERT DESCRIPTION AND DIMENSION', -1)

    DATA_GROUP_MARKERS = _create_key('MRKG', '3D marker data', 3)
    DATA_GROUP_FORCES = _create_key('FRCG', 'force plate calculated data', 1)
    DATA_GROUP_JOINTANGLES = _create_key('JANG', 'joint angles', 3)
    DATA_GROUP_JOINTVECTORS = _create_key('JVEG', 'joint centers', 3)
    JVEG = DATA_GROUP_JOINTVECTORS
    DATA_GROUP_BODYANGLES = _create_key('BANG', 'segment angles', 3)
    DATA_GROUP_BODYCM = _create_key('BCMG', 'segment centers of mass', 3)
    DATA_GROUP_BODYCMX = _create_key('BCXG', 'segment axis x', 3)
    DATA_GROUP_BODYCMY = _create_key('BCYG', 'segment axis y', 3)
    DATA_GROUP_BODYCMZ = _create_key('BCZG', 'segment axis z', 3)
    DATA_GROUP_EXTERNALFORCES = _create_key('FEXG', 'external forces', 3)
    DATA_GROUP_EXTERNALFORCESARM = _create_key('FARG', 'external moment arms', 3)
    DATA_GROUP_CONSTRAINTFORCES = _create_key('FCOG', 'constraint forces', 3)
    DATA_GROUP_CONSTRAINTTORQUES = _create_key('TCOG', 'constraint torques', 3)
    DATA_GROUP_MUSCLETORQUES = _create_key('TMTG', 'muscle torques', 3)
    DATA_GROUP_EXTERNALTORQUES = _create_key('TEXG', 'external torques', 3)
    DATA_GROUP_SEGROT = _create_key('SROG', 'segment rotations', 3)
    DATA_GROUP_JOINTROT = _create_key('JROG', 'joint rotations', 3)
    DATA_GROUP_GRAVFORCE = _create_key('FGRG', 'gravitational forces', 3)
    DATA_GROUP_TOTALFORCE = _create_key('FTOG', 'total forces', 3)
    DATA_GROUP_TOTALTORQUE = _create_key('TTOG', 'total torques', 3)
    DATA_GROUP_POWERS = _create_key('POWG', 'powers', 3)
    DATA_GROUP_SEGANGVEL = _create_key('SAVL', 'INSERT DESCRIPTION AND DIMENSION', -1)
    DATA_GROUP_JOINTANGVEL = _create_key('JAVL', 'INSERT DESCRIPTION AND DIMENSION', -1)

    DATA_GROUP_L_JOINTANGLES = _create_key('JANL', 'local joint angles', 3)
    DATA_GROUP_L_BODYANGLES = _create_key('BANL', 'local segment angles', 3)
    DATA_GROUP_L_EXTERNALFORCES = _create_key('FEXL', 'local external forces', 3)
    DATA_GROUP_L_EXTERNALFORCESARM = _create_key('FARL', 'local external moment arms', 3)
    DATA_GROUP_L_CONSTRAINTFORCES = _create_key('FCOL', 'local constraint forces', 3)
    DATA_GROUP_L_CONSTRAINTTORQUES = _create_key('TCOL', 'local constraint torques', 3)
    DATA_GROUP_L_MUSCLETORQUES = _create_key('TMTL', 'local muscle torques', 3)
    DATA_GROUP_L_EXTERNALTORQUES = _create_key('TEXL', 'local external torques', 3)

    # collections

    COLL_EMGGROUP = _create_key('CEMG', 'INSERT DESCRIPTION AND DIMENSION', -1)

    # internal Motion stuff

    DATA_GROUP_HDDN = _create_key('HDDN', 'Motion Internal Hidden Data', -1)


    @staticmethod
    def allow_unknown(callback):
        global add_unknown
        add_unknown = callback

    @staticmethod
    def get(magic: int):
        """
        Returns a registered MagicKey for the provide magic number.
        :param magic: Integer value of the MagicKey of interest.
        :return: The registered MagicKey object.
        :raise ValueError: A ValueError is raised in case for the magic number no MagicKey is found.
        """
        global add_unknown

        # TODO: This does not look like it will have a good performance.
        #       We should convert the set into a dict or hashmap.
        for mk in _magic_keys_dict:
            if mk.magic == magic:
                return mk
        if add_unknown:
            # Since this is an unknown magick key, we don't want to add them to the known keys.
            magic_key = _decode_magic_key(magic)
            add_unknown(magic_key)
            return MagicKey(magic_key, "Unknown key", -1)
        else:
            raise ValueError("The magic number '{}' is unknown".format(_decode_magic_key(magic)))

    @staticmethod
    def get_registered_keys():
        return sorted(_magic_keys_dict, key=lambda x: x.magic_str)

    @staticmethod
    def encode(cc):
        return _encode_magic_key(cc)

    @staticmethod
    def decode(cc):
        return _decode_magic_key(cc)

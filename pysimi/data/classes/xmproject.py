import xml.etree.ElementTree as Et
import base64
from pathlib import Path
from typing import Union
import numpy as np

from pysimi.data.classes.data_row import DataRow
from pysimi.data.classes.data_group import DataGroup
from pysimi.data.classes.projectbase import ProjectBase
from pysimi.data.classes.magickeys import MagicKey
from pysimi.data.classes.magickeys import MagicKeys
from pysimi.data.classes.phase_row import PhaseRow
from pysimi.data.classes.phase_group import PhaseGroup


def etree_findtext(root: Et.Element, search_str: str):
    res = root.findtext(search_str)
    if res is None:
        raise ValueError('No element "{}" found'.format(search_str))
    return res


def create_data_group(project, xmp_tree_data_group: Et.Element):
    data_group_id = int(etree_findtext(xmp_tree_data_group, ".//ID"))
    magic = MagicKeys.encode(etree_findtext(xmp_tree_data_group, ".//Magic"))
    name = xmp_tree_data_group.attrib['Name']
    track_count = int(etree_findtext(xmp_tree_data_group, ".//Tracks"))
    count = int(etree_findtext(xmp_tree_data_group, ".//Count"))

    return DataGroup(project, xmp_tree_data_group, data_group_id, MagicKeys.get(magic), name, track_count, count)


def create_phase_group(project, smlib_phase_group, phase_group_count, count):
    phase_group = PhaseGroup(project, smlib_phase_group, smlib_phase_group.get("Name"), phase_group_count, count)
    return phase_group


def create_data_row(self, data_group_parent: Et.Element, xmp_tree_data_row: Et.Element):
    data_group_id = int(etree_findtext(xmp_tree_data_row, ".//ID"))
    name = xmp_tree_data_row.attrib['Name']
    try:
        first = float(etree_findtext(xmp_tree_data_row, ".//First"))
        last = float(etree_findtext(xmp_tree_data_row, ".//Last"))
        size = int(etree_findtext(xmp_tree_data_row, ".//Size"))
        type_name = xmp_tree_data_row.findtext(".//TypeName")               # if value does not exist, set to None
        short_type_name = xmp_tree_data_row.findtext(".//ShortTypeName")    # if value does not exist, set to None
        unit_name = xmp_tree_data_row.findtext(".//UnitName")               # if value does not exist, set to None

        data_row = DataRow(self, data_group_parent, xmp_tree_data_row, name, type_name, short_type_name, unit_name,
                           data_group_id, size, first, last)
        return data_row
    except ValueError as ex:
        # Something went wrong when reading the data row properties. One reason might be linked data rows, which
        # contain only the reference information but not the actual data. In that case we want to return an empty
        # data row.
        if xmp_tree_data_row.findtext(".//LinkType"):
            return DataRow(self, data_group_parent, xmp_tree_data_row, name, "Linked Data Row",
                           "Linked", None, data_group_id, 0, 0.0, 0.0)
        else:
            raise ex
    except Exception as ex:
        raise RuntimeError("Could not create data row.") from ex


def create_phase_row(self,  data_group_parent: Et.Element, xmp_tree_data_row: Et.Element):
    name = xmp_tree_data_row.attrib['Name']
    start = float(etree_findtext(xmp_tree_data_row, ".//Start"))
    end = float(etree_findtext(xmp_tree_data_row, ".//End"))
    phase_row = PhaseRow(self, data_group_parent, xmp_tree_data_row, name, start, end)
    return phase_row


class DatasIterator:
    def __init__(self, project):
        self.project = project

    def __iter__(self):
        self.data_group = self.project.tree.findall('Project/ProjectData/Datas')
        self.data_group_count = int(etree_findtext(self.project.tree, 'Project/ProjectData/Count'))
        self.current_data_group = 0
        return self

    def __next__(self):
        if self.current_data_group < self.data_group_count:
            current = self.data_group[self.current_data_group]
            self.current_data_group += 1
        else:
            raise StopIteration()
        return create_data_group(self.project, current)


class PhaseIterator:
    def __init__(self, project):
        self.project = project

    def __iter__(self):
        self.phase_row_count = int(etree_findtext(self.project.tree, 'Project/ProjectPhases/Count'))
        self.current_phase_row = 0
        self.count = int(etree_findtext(self.project.tree, "Project/ProjectPhases/Phases/Count"))
        return self

    def __next__(self):
        if self.current_phase_row < self.phase_row_count:
            smlib_phase_group = self.project.tree.findall("Project/ProjectPhases/Phases")[self.current_phase_row]
            self.current_phase_row += 1
        else:
            raise StopIteration()
        return create_phase_group(self.project, smlib_phase_group, self.current_phase_row, self.count)


class XmProject(ProjectBase):
    """
    Provide several functions to load, read and manipulate xmp files.

    This should never be called directly! Use a Project element for obtaining a XmProject element.
    """
    def __init__(self, path: Union[Path, str]):
        super(XmProject, self).__init__(path)
        self.tree = Et.parse(str(path))

    def get_videos(self):
        """
        Collect all videos from project and return list of paths

        :return: list of all video paths
        """
        avs = []
        cams = self.tree.findall('Project/ProjectCameras/Cameras/Camera/AVIMotion')
        for cam in cams:
            avs.append(cam.text)
        return [Path(x) for x in avs]

    def get_data_group(self, magic: MagicKey):
        data_group_collection = []
        data_row_roots = self.tree.findall('Project/ProjectData/Datas')
        for data_row_root in data_row_roots:
            magic_data = etree_findtext(data_row_root, ".//Magic")
            magic_data = MagicKeys.encode(magic_data)
            if (magic is None) or (magic == MagicKeys.DATA_GROUP_NULL) or (magic.magic == magic_data):
                data_group = create_data_group(self, data_row_root)
                data_group_collection.append(data_group)
        return data_group_collection

    def walk_data_group(self):
        return DatasIterator(self)

    def get_data_row_by_index(self, impl_data: Et.Element, track_index: int, data_row_index: int):
        track_count = int(etree_findtext(impl_data, ".//Tracks"))
        if track_index >= track_count:
            raise ValueError('requested track index: {}; max track index: {}'.format(track_count, track_index - 1))
        count = int(etree_findtext(impl_data, ".//Count"))
        if data_row_index >= count:
            raise ValueError('requested data index: {}; max data index: {}'.format(data_row_index, count - 1))

        final_index = data_row_index * track_count + track_index
        data_row_xml = impl_data.findall('./Data')[final_index]
        return create_data_row(self, impl_data, data_row_xml)

    def get_data_row_by_id(self, impl_data: Et.Element, track_index: int, data_row_id: int):
        track_count = int(etree_findtext(impl_data, ".//Tracks"))
        if track_index >= track_count:
            raise ValueError('requested track index: {}; max track index: {}'.format(track_count, track_index - 1))

        data_row_xml = impl_data.findall("./Data/ID[.='" + str(data_row_id) + "']..")
        if len(data_row_xml) > track_index:
            data_row_xml = data_row_xml[track_index]
        elif len(data_row_xml) == 0:
            data_group_id = int(etree_findtext(impl_data, ".//ID"))
            raise ValueError('in data_group ID {}: no data found for given id: {}'.format(data_group_id, data_row_id))
        else:
            raise ValueError('requested track index: {}; max track index: {}'.format(track_count, track_index - 1))
        data_row = create_data_row(self, impl_data, data_row_xml)
        return data_row

    def get_data_row(self, impl_data: Et.Element):
        b64dat = etree_findtext(impl_data, ".//Base64")
        byts = base64.b64decode(b64dat + '===')
        data_row = np.frombuffer(byts, dtype=np.float32)
        return data_row

    def get_all_data_rows(self, group: Et.Element, data_row_id: int):
        data_rows = []
        data_group = group.findall("./Data/ID[.='" + str(data_row_id) + "']..")
        for dat in data_group:
            data_row = self.get_data_row(dat)
            data_rows.append(data_row)
        return np.stack(data_rows, 0).T

    def get_phase_group(self, index=0):
        phase_group_collection = []
        count = int(self.tree.findall("Project/ProjectPhases/Count")[0].text)
        phase_row_roots = self.tree.findall('Project/ProjectPhases/Phases')
        if index < count:
            for phase_row_root in phase_row_roots:
                phase_group = create_phase_group(self, phase_row_root, index, count)
                phase_group_collection.append(phase_group)
        else:
            raise IndexError("This Phase group with following {} index does not exist. "
                             "Number of PhaseGroups available are {}".format(index, count))
        return phase_group_collection

    def walk_phase_group(self):
        return PhaseIterator(self)

    def get_phase_row_by_index(self, impl_data: Et.Element, phase_row_index: int):

        count = int(etree_findtext(impl_data, ".//Count"))
        if phase_row_index >= count:
            raise ValueError('requested phase index: {}; max phase index: {}'.format(phase_row_index, count - 1))
        phase_row_xml = impl_data.findall('./Phase')[phase_row_index]
        return create_phase_row(self, impl_data, phase_row_xml)



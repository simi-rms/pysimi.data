import unittest
from pysimi.data.classes.magickeys import MagicKey
from pysimi.data.classes.magickeys import MagicKeys

MK_NULL = 0
MK_RAW = 542589266
MK_FPLC = 1129074758
MK_3D = 759444269


class TestMagicKey(unittest.TestCase):
    def test_init_magickey(self):
        magic_str = 'WD40'
        description = 'Solves every mechanical problem'
        tracks = 42
        test_el = MagicKey(magic_str, description, tracks)
        self.assertEqual(magic_str, test_el.magic_str)
        self.assertEqual(tracks, test_el.tracks)
        self.assertEqual(description, test_el.description)
        self.assertEqual(MagicKeys.encode(magic_str), test_el.__hash__())

    def test_for_equality(self):
        mk1 = MagicKey('RAW ', 'camera raw data', 2)
        mk2 = MagicKey('RAW ', 'camera raw data', 2)

        # Only a different description
        mk3 = MagicKey('RAW ', 'camera raw data', 2)

        # Only a different track count
        mk4 = MagicKey('RAW ', 'camera raw data', 2)

        self.assertEqual(mk1, mk2)
        self.assertEqual(mk1, mk3)
        self.assertEqual(mk1, mk4)

        self.assertTrue(mk1 == MK_RAW)

        with self.assertRaises(TypeError):
            mk1 == 0.42

    def test_less_than(self):
        # self.assertTrue(MagicKeys.DATA_GROUP_AMTI < MagicKeys.DATAS_RAW)
        # self.assertTrue(MagicKeys.DATA_GROUP_AMTI < MK_RAW)

        self.assertFalse(MagicKeys.DATA_GROUP_RAW < MagicKeys.DATA_GROUP_RAW)
        self.assertFalse(MagicKeys.DATA_GROUP_RAW < MK_RAW)

        with self.assertRaises(TypeError):
            MagicKeys.DATA_GROUP_RAW < 0.42


class TestMagicKeys(unittest.TestCase):
    def test_decoding(self):
        self.assertEqual('RAW ', MagicKeys.decode(MK_RAW))
        self.assertEqual('FPLC', MagicKeys.decode(MK_FPLC))
        self.assertEqual('-3D-', MagicKeys.decode(MK_3D))

    def test_encoding(self):
        # Using strings
        self.assertEqual(MK_RAW, MagicKeys.encode('RAW '))
        self.assertEqual(MK_FPLC, MagicKeys.encode('FPLC'))
        self.assertEqual(MK_3D, MagicKeys.encode('-3D-'))

        # Using character arrays
        self.assertEqual(MK_RAW, MagicKeys.encode(['R', 'A', 'W', ' ']))
        self.assertEqual(MK_FPLC, MagicKeys.encode(['F', 'P', 'L', 'C']))
        self.assertEqual(MK_3D, MagicKeys.encode(['-', '3', 'D', '-']))

        # Using value arrays
        self.assertEqual(MK_NULL, MagicKeys.encode([0, 0, 0, 0]))
        self.assertEqual(MK_RAW, MagicKeys.encode([ord('R'), ord('A'), ord('W'), ord(' ')]))
        self.assertEqual(MK_FPLC, MagicKeys.encode([ord('F'), ord('P'), ord('L'), ord('C')]))
        self.assertEqual(MK_3D, MagicKeys.encode([ord('-'), ord('3'), ord('D'), ord('-')]))

    def test_some_constants(self):
        self.assertEqual(MK_NULL, MagicKeys.DATA_GROUP_NULL.magic)
        self.assertEqual(MK_FPLC, MagicKeys.DATA_GROUP_FRCPLTCALC.magic)

    def test_get_registered_keys_are_sorted(self):
        mks = MagicKeys.get_registered_keys()

        self.assertEqual(MagicKeys.DATA_GROUP_NULL, mks[0])
        self.assertEqual(MagicKeys.DATA_GROUP_2D, mks[1])
        self.assertEqual(MagicKeys.DATA_GROUP_3D, mks[2])
        self.assertEqual(MagicKeys.DATA_GROUP_2DUSER, mks[3])
        self.assertEqual(MagicKeys.DATA_GROUP_USER, mks[-2])
        self.assertEqual(MagicKeys.DATA_GROUP_WRAW, mks[-1])

    def test_get_from_integer(self):
        self.assertEqual(MagicKeys.DATA_GROUP_NULL, MagicKeys.get(0))
        self.assertEqual(MagicKeys.DATA_GROUP_RAW, MagicKeys.get(MK_RAW))

        passed_magick = 0

        def blah(magick):
            nonlocal passed_magick
            passed_magick = magick

        MagicKeys.allow_unknown(blah)

        self.assertEqual(42, MagicKeys.get(42).magic)
        self.assertEqual('*', passed_magick[0])

        MagicKeys.allow_unknown(None)

        with self.assertRaises(ValueError):
            MagicKeys.get(21)


if __name__ == '__main__':
    unittest.main()

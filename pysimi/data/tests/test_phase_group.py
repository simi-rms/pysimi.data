# -*- coding: utf-8 -*-
#
#  Copyright © 2020 Simi Reality Motion Systems GmbH
#
from pysimi.data.classes.phase_group import PhaseGroup
from pysimi.data.classes.projectbase import ProjectBase

from mockito import when

import unittest


class TestPhaseGroup(unittest.TestCase):

    def test_initialization_properties(self):
        phase_group = PhaseGroup(None, None, "Phases", 1, 1)

        self.assertEqual("Phases", phase_group.name)
        self.assertEqual(1, phase_group.phase_group_count)
        self.assertEqual(1, phase_group.phase_row_count)

    def test_get_phase_row_by_index_calls_project(self):
        project = ProjectBase(path=None)
        when(project).get_phase_row_by_index("group", 1).thenReturn("found group 1")
        phase_group = PhaseGroup(project, "group", "Phases", 0, 11)
        self.assertEqual("found group 1", phase_group.get_phase_row_by_index(1))

    def test_get_phase_row_by_index_raises_exceptions(self):
        phase_group = PhaseGroup(None, None, "Phases", 1, 1)

        with self.assertRaises(IndexError):
            phase_group.get_phase_row_by_index(12)

    def test_walk_phase_row(self):
        project = ProjectBase(path=None)
        when(project).get_phase_row_by_index(...).thenReturn(1)
        impl_data = None
        phase_group = PhaseGroup(project, impl_data, "Phases", 1, 11)

        counter = 0

        for phase_row in phase_group.walk_phase_row(0):
            counter += phase_row

        self.assertEqual(11, counter)

    def test_walk_phase_row_raises_exception(self):
        phase_group = PhaseGroup(None, None, "Phases", 1, 1)

        with self.assertRaises(IndexError):
            phase_group.walk_phase_row(2)


if __name__ == '__main__':
    unittest.main()
